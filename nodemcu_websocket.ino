#include <ESP8266WiFi.h>        // Include the Wi-Fi library
#include <ESP8266mDNS.h>        // Include the mDNS library
#include <ESP8266WebServer.h>   // Include the WebServer library
#include <WebSocketsServer.h>

const char* ssid     = "FRITZ!Box 7530 GD";      // The SSID (name) of the Wi-Fi network you want to connect to
const char* password = "91946068241778199191";  // The password of the Wi-Fi network

ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80
WebSocketsServer webSocket(81);    // create a websocket server on port 81

void setup() {
  init_led();
  init_serial();
  init_spiffs();
  connect_wifi();
  start_mdns();
  init_server();
  start_websocket();
}

void loop() {
  MDNS.update(); // Without this MDNS does not work, see https://github.com/esp8266/Arduino/issues/4790
  server.handleClient();
  webSocket.loop();
}


// Web Socket

void start_websocket() { // Start a WebSocket server
  webSocket.begin();                          // start the websocket server
  webSocket.onEvent(webSocketEvent);          // if there's an incomming websocket message, go to function 'webSocketEvent'
  Serial.println("WebSocket server started.");
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) { // When a WebSocket message is received
  switch (type) {
    case WStype_DISCONNECTED:             // if the websocket is disconnected
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {              // if a new websocket connection is established
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
      }
      break;
    case WStype_TEXT:                     // if new text data is received
      Serial.printf("[%u] get Text: %s\n", num, payload);
      if (payload[0] == '#') {            // we get LED value
        uint32_t ledValue= (uint32_t) strtol((const char *) &payload[1], NULL, 10);   // decode value data
        Serial.printf("Decoded LED value: %d\n", ledValue);
        analogWrite(LED_BUILTIN, 255 - ledValue);
      }
      break;
  }
}


// Web Server

void init_server() {
  server.onNotFound([]() {                              // If the client requests any URI
    if (!handleFileRead(server.uri()))                  // send it if it exists
      server.send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
  });
  
  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");  
}

bool handleFileRead(String path) { // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  
  if (path.endsWith("/")) 
    path += "index.html";         // If a folder is requested, send the index file
  
  String contentType = getContentType(path);            // Get the MIME type
  
  if (SPIFFS.exists(path)) {                            // If the file exists
    File file = SPIFFS.open(path, "r");                 // Open it
    size_t sent = server.streamFile(file, contentType); // And send it to the client
    file.close();                                       // Then close the file again
    return true;
  }
  
  Serial.println("\tFile Not Found");
  return false;                                         // If the file doesn't exist, return false
}

String getContentType(String filename) { // convert the file extension to the MIME type
  if (filename.endsWith(".html")) 
    return "text/html";
  else if (filename.endsWith(".css")) 
    return "text/css";
  else if (filename.endsWith(".js")) 
    return "application/javascript";
  else if (filename.endsWith(".ico")) 
    return "image/x-icon";
  else if(filename.endsWith(".png")) 
    return "image/png";
  else if(filename.endsWith(".jpg")) 
    return "image/jpeg";
  else
    return "text/plain";
}

// Initialisation functions
void init_led() {
   pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
   digitalWrite(LED_BUILTIN, HIGH);  // Change the state of the LED
}

void init_serial() {
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');  
}

void init_spiffs() {
  SPIFFS.begin();
}

void connect_wifi() {
  WiFi.begin(ssid, password);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer  
}

void start_mdns() {
  if (!MDNS.begin("esp8266")) {            // Start the mDNS responder for esp8266.local
    Serial.println("Error setting up MDNS responder!");
  }
  Serial.println("mDNS responder started");
}
