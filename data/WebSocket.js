var rainbowEnable = false;
var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']);
connection.onopen = function () {
    connection.send('Connect ' + new Date());
};
connection.onerror = function (error) {
    console.log('WebSocket Error ', error);
};
connection.onmessage = function (e) {
    console.log('Server: ', e.data);
};
connection.onclose = function(){
    console.log('WebSocket connection closed');
};

function sendValue() {
    var value = document.getElementById('ledValue').value;
    var valueStr = '#'+ value.toString();
    console.log('Led value: ' + valueStr);
    connection.send(valueStr);
}
